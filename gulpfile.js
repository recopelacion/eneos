var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
// var uglify = require('gulp-uglify');
// var imagemin = require('gulp-imagemin');

gulp.task('browserSync', function(){
  
  browserSync.init({
    server: "./"
  });
    gulp.watch('raw-assets/sass/**/*.scss', gulp.series('sass'));
    gulp.watch('*.html', gulp.series('html'));
    // gulp.watch('raw-assets/js/**/*', gulp.series('scripts'));
    // gulp.watch('raw-assets/img/**/*', gulp.series('imagemin'));
});

gulp.task('sass', function () {
  return gulp.src('raw-assets/sass/**/*.scss')
    .pipe(sass({
        outputStyle: 'expanded'
      }).on('error', sass.logError))
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.stream());
});

gulp.task('html', function () {
  return gulp.src('*.html')
    .pipe(browserSync.stream());
});

// gulp.task('scripts', function() {
//   return gulp.src('raw-assets/js/**/*')
//     // Minify the file
//     .pipe(uglify())
//     // Output
//     .pipe(gulp.dest('assets/js/'))

//     .pipe(browserSync.stream());
// });

// gulp.task('imagemin', function() {
//     return gulp.src('raw-assets/img/**/*')
//            .pipe(imagemin({
//                 progressive: true
//            }))
//            .pipe(gulp.dest('assets/img/'))
// });
    
gulp.task('default', gulp.series('browserSync'));